<?php
namespace App\models;

include_once "includes/Data.php";
use Frame\Data;

class User extends Data {
    
    /**
     * @var string $login User login
     */
    private string $login = 'guest';
    
    /**
     * @var string $SSID User SSID
     */
    private string $SSID = '';
    
    /**
     * @var array $roles User roles
     */
    private array $roles = [];

    /**
     * @var string $email User email
     */
    private string $email = '';
    
    /**
     * @var string $passwd User hashed password
     */
    private string $passwd = '';

    /**
     * 
     */
    public function __construct()
    {
        parent::__construct($this->login);
    }

    /**
     * Get $login User login
     *
     * @return  string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set $login User login
     *
     * @param  string  $login  $login User login
     *
     * @return  self
     */
    public function setLogin(string $login)
    {
        $this->login = $login;

        return $this;
    }
}