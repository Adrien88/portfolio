<?php
namespace App\models;

include_once "includes/Data.php";
use Frame\Data;

class Page extends Data {
    
    /**
     * 
     */
    private string $filename = 'default.html';

    public function __construct()
    {
        parent::__construct($this->filename);
    }

    /**
     * Get the value of filename
     *
     * @return  string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set the value of filename
     *
     * @param  string  $filename
     *
     * @return  self
     */
    public function setFilename(string $filename)
    {
        $this->filename = $filename;

        return $this;
    }
}