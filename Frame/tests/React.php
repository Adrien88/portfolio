<?php

namespace React;

ini_set('display_errors', "1");

define('CACHENAME', 'TemporaryPseudonameForMainReact');
define('CACHEPATH', __DIR__ . '/' . CACHENAME . '.php');
${CACHENAME} = [];

/**
 * Get an unique string key randmoly (maybe ?)
 * 
 * @param int lenght [3] Default lenght parts string
 * @param string
 */
function getKey(int $lenght = 3): string
{
    $str = '';
    for ($i = 0; $i < $lenght; $i++)
        $str .= md5(microtime()) . '-';
    return substr($str, 0, -1);
}

/**
 * getCurrentName
 * 
 * @param void
 * @return null|string
 */
function getCurrentFuncName(): null|string
{
    $data = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 3);
    if (isset($data[1]))
        return $data[1]['function'];
    return null;
}

/**
 * getParentName
 * 
 * @param void
 * @return null|string
 */
function getParentFuncName(): null|string
{
    $data = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 3);
    if (isset($data[2]))
        return $data[2]['function'];
    return null;
}

/**
 * Set new started element
 * 
 * @param mixed obj
 * @return string key
 */
function setNewStatedElement(mixed $obj): string
{
    $key = getKey(1);
    global ${CACHENAME};
    ${CACHENAME}[$key] = [
        'type' => gettype($obj),
        'content' => $obj
    ];
    return $key;
}

/**
 * issetStatedElement 
 * 
 * @param string key
 * @return bool
 */
function issetStatedElement(string $key): bool
{
    global ${CACHENAME};
    return isset(${CACHENAME}[$key]);
}

/**
 * deleteStatedElement
 * 
 * @param string key
 * @return void
 */
function deleteStatedElement(string $key): void
{
    global ${CACHENAME};
    unset(${CACHENAME}[$key]);
}

namespace React\PHPminifier;

function getHeredocPatterns(string $doc)
{
    // foreach (findHeredocPattern($doc) as $groups) {
    // }
    // return '/\<\<\<'.$keyword.' (.*) '.$keyword.'\;/xis';
}

function findHeredocPatterns(string $doc)
{
    $patterns = [];
    $matched = [];
    preg_match_all('/<<<([\w]+)/xis', $doc, $matches, PREG_SET_ORDER);
    foreach ($matches as $heredoc)
        $patterns[] = '/' . $heredoc[0] . ' (.*) ' . $heredoc[1] . ';/xis';
    foreach ($patterns as $pattern) {
        preg_match($pattern, $doc, $matched);
        $rep = $matched[0];
        $by = cleanString(deleteChars($matched[1]));
        $doc = strtr($doc, [$rep => $by]);
    }
    var_dump($doc);

    // return $matched;
}


function cleanString(string $str): string
{
    return '"' . preg_replace(['/[ ]{2,}/', '/\\/', '/"/'], [' ', '\\\\', '\"'], $str) . '";';
}

function deleteChars(string $str, array $chars = ["\t" => '', "\n" => '']): string
{
    return strtr($str, $chars);
}

function deletesComments(string $php)
{
}

function minifyPHP(string $php): string
{
    $php = deleteChars($php);
    return str_replace("<?php", '', $php);
    // $php = preg_replace('/^<\?php/is', '', $php);
}

namespace React\AppBuilder;

function linker(array $folders)
{
    foreach ($folders as $folder);
}

function fileMapper(string $current, callable $callable)
{
    foreach (glob($current . '/*') as $file)
        if (is_dir($file))
            fileMapper($file, $callable);
        else
            $callable($file);
}

/**
 * React console
 */
// namespace React\bin;

// function console(string $funcname, array $props = []){

// }

/**
 * React hooks
 */

namespace React\hook;

/**
 * @return array[] [ $obj, $callalbe() ]
 */
function useState(mixed $obj): array
{
    // $funcname = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2)[1]['function'];
    $key = \React\setNewStatedElement($obj);
    global ${CACHENAME};
    return [
        function () use (&$key) {
            global ${CACHENAME};
            return ${CACHENAME}[$key]['content'];
        },
        function ($obj) use (&$key) {
            global ${CACHENAME};
            if (gettype($obj) === ${CACHENAME}[$key]['type']) {
                ${CACHENAME}[$key]['content'] = $obj;
            }
        }
    ];
}

// function useEffect(){
// }

namespace App;

/**
 * Exemple
 */
function foo()
{
    [$getObject, $setObject] = \React\hook\useState(new \stdClass);
    [$getValue, $setValue] = \React\hook\useState(10);

    $setValue(24);  // ok
    $setValue('12'); // impossible

    var_dump($getValue()); // 11
    var_dump($getObject()); // object(stdClass)#1 (0) {}
}
foo();




/**
 * testing minifier catcher
 */
$var = <<<HTML
        <html>
            <head action="">
                <title>Test</title>
            </head>
        </html>
    HTML;

$var = <<<SQL
    INSERT INTO database.users values (
        (null 'login', 'passwd'),
        (null 'login', 'passwd')
    ); 
    SQL;

$getted = \React\PHPminifier\findHeredocPatterns(file_get_contents(__FILE__));
// var_dump($getted);

// function A()
// {
//     // echo \React\getCurrentName()."\n";
//     // echo \React\getParentName()."\n";
// }
// function B()
// {
//     A();
// }
// B();
