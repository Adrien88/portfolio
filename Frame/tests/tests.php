<?php

namespace Frame;

ini_set('display_errors', "1");

namespace Frame\Service;

use ArrayObject;
use Reflection;
use ReflectionAttribute;
use ReflectionClass;
use ReflectionFunction;
use ReflectionMethod;

namespace Frame\EventDispatcher; 

// class EventList {
//     function add(){}
//     function get(){}
// }

// class listenner{
//     function __construct(string $eventname, callable $callable, EventList $eventList){
//         $this->eventname = $eventname;
//         $this->callable = $callable;
//         $eventList->add($eventname, $this);
//     }
// }

class Provider extends Scanner {

    function __construct(){
    }

    function setInstance(string $caller)
    {
        if (isset($this->collection[$caller])) {
            /**
             * @var ReflectionClass reflection
             */
            $reflection = $this->collection[$caller]['reflection'];
            if ($reflection->isInstantiable()) {

                $params = $reflection->getConstructor()->getParameters();
                /**
                 * consctruct recursvly
                 */
                foreach($params as $key => $param){
                    foreach($reflection->getAttributes() as $attr){
                        // if attr is inject elemnt
                        if ($attr->getName() === $param->gettype()){
                            $param[$key] = $attr->newInstance; 
                        }
                        // else if attr is current class
                        else if ($this->classCollection[$param->gettype()]){
                            
                        } 
                        // try to build default.
                        else {

                        }
                    }
                }

                $instance = $reflection->newInstance(...$params);
                $this->collection[$caller]['instance'] = $instance;
            }

        }
    }

    function callInstance(string $caller): object
    {
        if (isset($this->collection[$caller])) {
            if (!isset($this->collection[$caller]['instance']))
                $this->setInstance($caller);
            return $this->collection[$caller]['instance'];
        }
    }

    function callMethod(string $class, string $method)
    {
        if (isset($this->collection[$class . '::' . $method])) {
            $obj = $this->callInstance($class);
            $obj->$method();
        }
    }

    function callFunc(string $caller)
    {
    }
}

class Scanner
{
    /**
     * 
     * @var ArrayObject
     */
    public \ArrayObject $classCollection;
    
    /**
     * 
     * @var ArrayObject
     */
    public \ArrayObject $functionCollection;

    /**
     * namespace\path\function
     * namespace\path\classname::method
     * 
     * @return self 
     */
    public function __construct()
    {
        $this->classCollection = new \ArrayObject;
        $this->functionCollection = new \ArrayObject;
        $this->scanUserClasses();
        $this->scanUserFunctions();
        // var_dump($this->collection);
    }

    function scanUserClasses()
    {
        foreach (get_declared_classes() as $classname) {
            $reflectionClasse = new \ReflectionClass($classname);
            if ($reflectionClasse->isUserDefined()) {
                $classname = $reflectionClasse->getName();
                $this->classCollection[$classname]['reflection'] = $reflectionClasse;
                foreach ($reflectionClasse->getMethods() as $method) {
                    $methodname = $classname . '::' . $method->getName();
                    $this->classCollection[$methodname]['reflection'] = $method;
                }
            }
        }
    }

    function scanUserFunctions()
    {
        foreach (get_defined_functions()['user'] as $function) {
            $reflection = new \ReflectionFunction($function);
            $this->functionCollection[$reflection->getName()]['reflection'] = $reflection;
        }
    }

}



class Listenners
{
    function __construct(
        string $classname,
        ?string $method
    ) {
    }
}


/**
 *      ----------------------------------------
 *      ----------------------------------------
 *      ----------------------------------------
 *      ----------------------------------------
 *      ----------------------------------------
 */

namespace Frame;

use Frame\http\Request;
use Frame\Service\Emmiters;
use Frame\Service\Listenners;
use Frame\Service\Provider;

// #[Router(Provider::class, Request::class)]
class Router
{
    function __construct(
        string $path,
        Request $request,
    ) {
        // get prefix
        // $ controller = ServiceProvider::getContext('App\Controllers');
        # get 
    }
}


#[\Attribute(\Attribute::TARGET_METHOD)]
class Route
{
    // static $list = [];
    function __construct(
        // public string $path,
        // public string $name,
        // public array $methods,
    ) {
        // self::$list[$name] = $this;
    }
}


#[\Attribute(\Attribute::TARGET_CLASS)]
class Controller
{
    // static $list = [];
    function __construct(
        private string $prefix
    ) {
        //     self::$list[] = $this;
    }
}


/**
 *      ----------------------------------------
 *      ----------------------------------------
 *      ----------------------------------------
 *      ----------------------------------------
 *      ----------------------------------------
 */

namespace App;

use Frame\Route;
use Frame\Controller;

#[Controller(prefix: '/page')]
class PageController
{
    #[Route('/home', 'home')]
    function home()
    {
        echo "home";
    }
}

#[Controller(prefix: '/user')]
class UserController
{
    #[Route('/user', 'user')]
    function user()
    {
        echo "user";
    }
}


namespace foo;
use ReflectionFunction;

class Crass {

}

function bar(Crass $MyCrass)
{
    var_dump($MyCrass);
}

$funcname = '\foo\bar';
// get firt param
$params = (new ReflectionFunction($funcname))->getParameters();
foreach($params as $key => $param){
    // $params[$key] = ('foo\bar' === (string)$param->gettype()) ? $param->newInstance() :  $param;
}
// var_dump($param->getName());          // string MyCrass
// var_dump((string)$param->gettype());  // string foo\Crass
// var_dump($params);
$funcname(...$params);

// new \Frame\EventDispatcher\Provider;
