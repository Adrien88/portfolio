```php

namespace foo;
use ReflectionFunction;

class Crass {
}

function bar(Crass $MyCrass)
{
    var_dump($MyCrass);
}

$funcname = '\foo\bar';
// get firt param
$params = (new ReflectionFunction($funcname))->getParameters();
foreach($params as $key => $param){
    $param[$key] = if('foo\bar' === (string)$param->gettype()) $param->newInstance();
}
// var_dump($param->getName());          // string MyCrass
// var_dump((string)$param->gettype());  // string foo\Crass
// var_dump($params);
$funcname(...$params);


```