<?php
namespace Frame;

class PDOHandler
{
    static array $config = [
        'dev' =>[
            "user" => "dev",
            "pwd" => "dev",
            "dsn" => "mysql:host=127.0.0.1:3306;dbname=portfolio",
        ],
        'prod' =>[
            "user" => "adrienb764",
            "pwd" => "6Xfzx7ZgzTfcoA==",
            "dsn" => "mysql:host=promo-97.codeur.online;dbname=adrienb764_portfolio",
        ],
    ];

    /**
     * @var PDO[] $conn Connexion list
     */
    private array $conn = [];

    function __constrcut()
    {
        if ("127.0.0.1" === $_SERVER['SERVER_NAME']){
            $this->createPDO(...self::$config['dev']);
        } 
        else {
            $this->createPDO(...self::$config['prod']);
        }
    }

    function getPDO(){
        return $this->conn['PDO'];
    }


    function createPDO($dsn, $user, $pwd)
    {
        try {
            $this->conn = [
                'conf' => [$dsn, $user, $pwd],
                'PDO' => new \PDO($dsn, $user, $pwd),
                'log' => [0 => "New conn the ".date('d/m/Y - H:i:s').'.'],
            ];
        }
        catch(\PDOException $e){
            var_dump($e);
        }
    }

}