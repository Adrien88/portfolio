<?php 

namespace Frame\router;

final class Route
{
    public function __construct(
        private string $path = '/',
        private string $name = 'root',
        private array $methodes = ['GET'],
    ){
    }
    final public function getPath(){
        return $this->path;
    }
    final public function getName(){
        return $this->name;
    }
    final public function getMethods(){
        return $this->methodes;
    }
}