<?php

namespace Frame\router;

use ReflectionClass;
use ReflectionMethod;

class Router
{
    static array $config = [
        'api' => [
            'extern_enabled' => true,
            'local_enabled' => true,
            'slug' => 'api',
            'default' => 'App/src/assets/html/index.html',
        ],
        'default' => [
            'controller' => 'App\controllers\Page',
            'method' => 'index',
            'args' => ['filename' => '404.html'],
        ]
    ];

    public function __construct()
    {
        $this->apiPreRouting();
    }

    private function apiPreRouting()
    {
        $path = $_SERVER['PATH_INFO'] ?? '/';
        if ('/' === $path || false === strpos($path, '/api', 0)) {
            $this->apiLanding($path);
        } else {
            $this->routing($path);
        }
    }

    private function apiLanding(string $path)
    {
        $conf = 'var config = {init:"' . $path . '"};';
        $model = file_get_contents(self::$config['api']['default']);
        $model = str_replace('//config_php_here', $conf, $model);
        echo $model;
    }



    private function prepare(array $Path)
    {
        $conf = self::$config['default'];
        
        $callClasse = "\App\controllers\\$Path[0]";
        if (class_exists($callClasse, true)) {
            array_shift($Path);
        } else {
            $callClasse = $conf['controller'];
        }
        if (method_exists($callClasse, $Path[0])) {
            $callMethod = array_shift($Path);
        } else {
            $callMethod = $conf['controller'];
        }
        $arg = array_merge($conf['args'], $Path);
        return [$callClasse, $callMethod, $arg];
    }

    private function routing(string $path){
        $path = explode('/', substr($path, 5));
        [$callClass, $callMethod, $args ] = $this->prepare($path);
        [$callClass, $callMethod, $args ] = $this->getController($callClass, $callMethod, $args);
        if ($callClass->isInstanciable()){

        }
    }

    function getController($controller, $method, $args)
    {
        if (class_exists($controller)) {
            $class = new ReflectionClass($controller);
            if ($class->hasMethod($method)){
                $method = $class->getMethod($method);
                $attrs = $method->getAttributes() ?? [];
                $params = $method->getParameters() ?? [];
            }
        }
        return [$class, $method->name, $params, $attrs, $args];
    }
}
