<?php

namespace Frame\autoload;

use Exception;

final class ClassLoader
{
    /**
     * @var string $folder Cache folder
     */
    const cache = "var/cache/autoloader.inc";

    static self $instance;

    private array $namespace = [
        'App' => 'App/src/includes',
        'Frame' => 'Frame/src/includes'
    ];

    private array $classList = [];

    public static function selfRegistre()
    {
        if (!isset(self::$instance))
            self::$instance = new self;
        spl_autoload_register([self::$instance, 'registre'], true, true);
    }

    final public function __construct()
    {
        $this->checkPath();
    }

    private function checkPath(): bool
    {
        $dir = dirname(self::cache);
        if (!file_exists($dir) && !mkdir($dir)) {
            throw new \Exception("Warning, PHP can\'t read/create autoload folder<b>\"" . $dir . "\"</b>");
            return false;
        }
        if (!file_exists(self::cache) && !file_put_contents(self::cache, "<?php return[];")) {
            throw new \Exception("Warning, PHP can\'t read/create autoload file.<br><b>\n" . self::cache . "<b>");
            return false;
        }
        return true;
    }

    private function save()
    {
        $str = '<?php return[';
        $array = [
            'updatedAt' => date('d:m:Y'),
            'autoload' => $this->classList
        ];
        $str .= $this->format($array);
        $str .= '];';
        if (false === file_put_contents(self::cache, $str))
            throw new Exception('Autoloader can\'t save cache.');
    }

    private function format(array &$array)
    {
        $str = '';
        foreach ($array as $key => $value) {
            $value = (is_array($value)) ? '[' . $this->format($value) . '],' : "\"$value\",";
            $key = (is_string($key)) ? "\"$key\"=>" : '';
            $str .= "$key$value";
        }
        return $str;
    }

    private function registre(string $classname)
    {
        foreach ($this->namespace as $namespace => $folder) {
            if (false !== strpos($classname, $namespace, 0)) {
                $path = str_replace(['\\', $namespace], ['/', $folder], $classname) . '.php';
                if (file_exists($path)) {
                    including($path);
                    $this->classList[$namespace][] = $path;
                    $this->save();
                    return;
                }
            }
        }
    }
}
