<?php

namespace Frame\autoload;

use TypeError;

final class Autoloader
{
    /**
     * @var string cache AutoloaderCacheFile
     */
    const cache = "var/cache/autoloader.inc";

    /**
     * Production autoloading
     * 
     * @return void 
     */
    static function autoload()
    {
        if (file_exists(self::cache)) {
            foreach ((include self::cache)['autoload'] as $filename)
                including(...$filename);
        }
    }

    /**
     * later : DEV only
     * 
     * @return void 
     * @throws TypeError 
     */
    static function callClassLoader()
    {
        include_once __DIR__ . "/ClassLoader.php";
        ClassLoader::selfRegistre();
    }
}

/**
 * Externalized icluding context.
 * 
 * @param mixed $filenames 
 * @return void 
 */
function including(...$filenames): void
{
    foreach ($filenames as $filename)
        if (file_exists($filename))
            include $filename;
}
