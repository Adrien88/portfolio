<?php

use Frame\ClassLoader;
use Frame\QuickReflection;
use Frame\servicesprovider\container;

class FactoryBuilder
{
    private array $callables = [];

    function __construct(string $classname){
        foreach(get_class_methods($classname) as $name)
        
        $this->callables['build'.ucfirst($name)]  = [
            'attributes' => QuickReflection::getMethodeAttributes($classname, $name),    
            'parameters' => QuickReflection::getMethodeParameters($classname, $name),    
        ];
    }

    function __call($name, $args){
        if(array_key_exists($name, $this->callables)){
            $call = $name;
        }
    }
}

$factory = (new FactoryBuilder(ClassLoader::class));
//     ->setFactoryName('MaClass')
//     ->setDefaultModel(MaClass::class)
//     ->setDefaultParameters(name:'foo')
//     ->setDefaultContainer(Container $container);