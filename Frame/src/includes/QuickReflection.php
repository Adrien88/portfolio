<?php

namespace Frame;

use ReflectionClass;
use ReflectionFunction;

/**
 * Quick reflection class call.
 * 
 * ex: QuickReflection::getMethodeAttributes(Class::class, 'foo');
 * 
 */
class QuickReflection {
    
    /**
     * 
     */
    // final static function   


    /**
     * getClassAttributes
     * 
     * @param object|string $context classname or object
     * 
     * @return array
     */
    final static function getClassAttributes(string|object $context): array
    {
        $class = (is_object($context)) ? get_class($context) : $context;
        if (isset($class) && class_exists($class, true))
            return (new ReflectionClass($class))
                ->getAttributes();
        else
            return [];
    }
    
    /**
     * getMethodeAttributes
     * 
     * @param object|string $context classname or object
     * @param string $method
     * 
     * @return array
     */
    final static function getMethodeAttributes(string|object $context, string $method): array
    {
        $class = (is_object($context)) ? get_class($context) : $context;
        if (class_exists($class, true) and method_exists($class, $method))
            return (new ReflectionClass($class))
                ->getMethod($method)
                ->getAttributes();
        else
            return [];
    }
    
    /**
     * getMethodeParameters
     * 
     * @param object|string $context classname or object
     * @param string $method
     * 
     * @return array
     */
    final static function getMethodeParameters(string|object $context, string $method): array
    {
        $class = (is_object($context)) ? get_class($context) : $context;
        if (class_exists($class, true) and method_exists($class, $method))
            return (new ReflectionClass($class))
                ->getMethod($method)
                ->getParameters();
    
        else
            return [];
    }
    
    /**
     * getFunctionAttributes
     * 
     * @param string $funcname
     * 
     * @return array
     */
    final static function getFunctionAttributes(string $funcname): array
    {
        if (function_exists($funcname))
            return (new ReflectionFunction($funcname))
                ->getParameters();
        return [];
    }
    
    /**
     * getFunctionParameters
     * 
     * @param string $funcname
     * 
     * @return array
     */
    final static function getFunctionParameters(string $funcname): array
    {
        if (function_exists($funcname))
            return (new ReflectionFunction($funcname))
                ->getAttributes();
        return [];
    }
}
