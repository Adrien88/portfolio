<?php 

namespace Frame\eventdispatcher;

/**
 * SubscriberInterface Default subscriber behavior 
 */
interface SubscriberInterface {
    

    /**
     * @return string The event name
     */
    function getEventName(): string;
    
    /**
     * @return callable The callables
     */
    function getEventCallable(): callable;
    
    /**
     * @return array The parameters
     */
    function getEventParameters(): array;
}