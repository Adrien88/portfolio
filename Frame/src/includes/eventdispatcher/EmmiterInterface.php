<?php 

namespace Frame\eventdispatcher;

/**
 * SubscriberInterface Default subscriber behavior 
 */
interface EmmiterInterface {
    
    /**
     * @return string The event name
     */
    function getEventName(): string;

    /**
     * @return array The parameters
     */
    function getEventParameters(): array;
}