<?php

namespace Frame\eventdispatcher;

class Listenner
{

    public function __construct(
        private string $eventName
    ) {
    }

    public function getName(){
        return $this->eventName;
    }

    public function addSubscriber(SubscriberInterface $subscriber)
    {
        $this->list[] = $subscriber;
    }
}
