<?php 

namespace Frame\eventdispatcher;

class EventManager {

    static self $instance;

    /**
     * 
     */
    private array $events = [];

    /**
     * getInstance
     * 
     * @param $params[] The paramaters : see:__construct() 
     * @return self 
     */
    public static function getInstance(...$params){
        if (null === self::$instance)
            self::$instance = new self(...$params);
        return self::$instance;
    } 

    private function __construct(){}
    
    function addEvent(Event $event, bool $override = false): bool 
    {
        // if (!$this->existEvent($event->getName()) || true === $override){
        //     $this->events[$event->getName()] = $event;
        //     return true;
        // }
        return false;
    }
    
    function getEvent(string $name): Event
    {
        return $this->events[$name];
    }
    
    function removeEvent(string $name): void 
    {
        unset($this->events[$name]);
    }
    
    function existEvent(string $name): bool
    {
        return isset($this->events[$name]);
    }
}