<?php

namespace Frame\eventdispatcher;

class ListennerCollection
{
    public function __construct(
        private string $collectionName
    ) {
    }

    public function add(Listenner $listener)
    {
        $this->collection[$listener->getName()] = $listener;
    }

    public function addSubscriber(string $name): Listenner
    {
        return $this->collection[$name];
    }
}
