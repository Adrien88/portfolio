<?php

ini_set('display_errors', "1");
define("DEV", true);
include "Frame/src/includes/autoload/Autoloader.php";
\Frame\autoload\Autoloader::autoload();
\Frame\autoload\Autoloader::callClassLoader();

new Frame\security\User();
new Frame\router\Router();
