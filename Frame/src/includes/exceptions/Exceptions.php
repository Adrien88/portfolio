<?php

namespace Frame\exceptions;

class Exception extends \Exception // implements FrameShemas // <- voir creer schemas http system
{
    function toJson()
    {
        return json_encode([
            'message' => $this->getMessage(),
            'code' => $this->getCode(),
            'file' => $this->getFile(),
            'line' => $this->getLine(),
            'traces' => $this->getTrace(),
        ], JSON_PRETTY_PRINT);
    }

    function toHtml()
    {
        return "
        <div class=\"FrameException\">
            <div class=\"message\">".$this->getMessage()."</div>
            <div class=\"code\">".$this->getCode()."</div>
            <div class=\"trace\">".$this->getTrace()."</div>
        </div>";
    }
}
