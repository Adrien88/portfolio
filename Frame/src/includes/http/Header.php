<?php

namespace Frame\http;

final class Header
{
    private array $headers = [];

    final public function __construct()
    {
        $this->headers = [];
    }

    final public function set(string $name, string $value): void
    {
        $this->headers[$name] = $value;
    }

    final public function append(string $name, string $value): void
    {
        $this->headers[$name] .= $value;
    }

    final public function get(string $name): string
    {
        return $this->headers[$name];
    }

    final public function unset(string $name): void
    {
        unset($this->headers[$name]);
    }

    final public function apply(): void
    {
        foreach($this->headers as $name => $value)
            header("$name: $value", true);

    }
}
