<?php

namespace Frame\http;

use Frame\Data;
use stdClass;

final class Response extends Data
{
    public function __construct(string $filename = '/', Header $header)
    {
        parent::__construct(md5($filename));
        $this->code = 200;
        $this->content = null;
    }

    function __destruct()
    {
        
        echo $this->content;
    }
}
