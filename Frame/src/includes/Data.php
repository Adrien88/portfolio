<?php

namespace Frame;

use Exception;

class Data
{
    const data = "var/data/";
    const ext = ".dat";

    private string $filename;
    private array $data;

    public function __construct(string $name)
    {
        $subfolder = str_replace('\\', DIRECTORY_SEPARATOR, get_class($this));
        $this->filename = self::data . '/' . $subfolder . '/' . $name . self::ext;
        if (file_exists($this->filename))
            $this->data = unserialize(file_get_contents($this->filename));
    }

    function __set(string $key, $value):void
    {
        $this->data[$key] = $value;
    }

    function __get(string $key)
    {
        return $this->data[$key];
    }

    function __isset(string $key):bool
    {
        return isset($this->data[$key]);
    }

    function __unset(string $key):void
    {
        unset($this->data[$key]);
    }

    final public function save(): void
    {
        $dir = dirname($this->filename);
        if (!file_exists($dir) && !mkdir($dir, 0766, true))
            throw new Exception('PHP can\'t write folder');
        else
            file_put_contents($this->filename, serialize($this->data));
    }
}
