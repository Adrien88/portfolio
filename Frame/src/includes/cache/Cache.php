<?php

namespace Frame\cache;

final class Cache
{

    const folder = "var/cache/";

    private APCU|File $storage;
    private $ttl;

    final public function __construct(string $cachename = 'default', int $ttl = 0)
    {
        if (APCU::isAvailable())
            $this->storage = new APCU($cachename);
        else
            $this->storage = new File(self::folder . $cachename);
    }



    final public function set(int|string $name, mixed $value, int $ttl = 0): bool
    {
        return $this->storage->set($name, $value, $ttl);
    }
    
    final public function get(int|string $name): mixed
    {
        return $this->storage->get($name);
    }

    final public function isset(int|string $name): bool
    {
        return $this->storage->isset($name);
    }

    final public function unset(int|string $name): void 
    {
        $this->storage->unset($name);
    }

    final public function dump(int|string $name): array 
    {
        return $this->storage->dump($name);
    }

    final public function save(){
        $this->storage->save();
    }
}