<?php

namespace Frame\cache;

use Exception;

class File
{
    private array $data;
    private string $filename;

    public function __construct(string $filename = 'default')
    {
        $this->filename = $filename . '.inc';
        $this->checkPath();
        $this->load();
    }

    public function set(int|string $name, mixed $content): void
    {
        $this->data[$name] = $content;
    }

    public function get(int|string $name): mixed
    {
        return $this->data[$name];
    }
    
    public function isset(int|string $name): bool
    {
        return isset($this->data[$name]);
    }

    public function unset(int|string $name): void
    {
        unset($this->data[$name]);
    }

    public function clear(): bool
    {
        $this->data = [];
        return unlink($this->filename);
    }

    private function checkPath(): bool
    {
        if (!file_exists(dirname($this->filename)) && !mkdir(dirname($this->filename))) {
            throw new \Exception("Warning, PHP can\'t build autoload folder<b>\"".dirname($this->filename)."\"</b>");
            return false;
        }
        if (!file_exists($this->filename) && !file_put_contents($this->filename, "<?php return[];")) {
            throw new \Exception("Warning, PHP can\'t build autoload file.<br><b>\n".$this->filename."<b>");
            return false;
        }
        return true;
    }

    final public function dump(): array
    {
        return $this->data;
    }

    /**
     * 
     */
    public function save()
    {
        $string = '<?php return' . self::formatArray($this->data) . ';';
        file_put_contents($this->filename, $string);
    }

    public function load()
    {
        if (file_exists($this->filename))
            $this->data = include $this->filename;
            
    }

    public function packData($mixed): string
    {
        switch(gettype($mixed)){
            case 'integer':
            case 'double':return $mixed; break;
            case 'string':return '"'.str_replace(['\\','"'],['\\\\','\\"'],$mixed).'"'; break;
            // case 'object':return serialize($mixed); break;
            default: throw new Exception('Data format not supported.'); break;
        }
    }

    /**
     * stringify array
     */
    public static function formatArray(array $array = []): string
    {
        $str = '[';
        self::recursiveConstextualizedMapping(function ($value, $context) use (&$str) {
            $tab = str_repeat("\n", count($context));
            $key = (is_string(end($context))) ? '"' . end($context) . '"' : end($context);
            $value = $this->packData($value);
            $str .= "$tab$key => $value,\n";
            
        });
        return $str . ']';
    }

    /**
     * recursiveConstextualizedMapping
     * 
     * @param callable
     * @param context &$context [Autofilled] : stack of key from root to current key.
     * @param array &$array [Autofilled] : current browsed array.
     * 
     * @return void
     */
    public static function recursiveConstextualizedMapping(callable $callable = null, array $context = [], array &$array = []): void
    {
        foreach ($array as $key => $value) {
            $context[] = $key;
            if (is_array($value))
                $value = self::recursiveConstextualizedMapping($callable, $context, $value);
            $callable($value, $context);
        }
    }
}
