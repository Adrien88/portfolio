<?php

namespace Frame\cache;

use Exception;

final class APCU
{

    /**
     * @static isAvailable() Retuurn availability of APCu.
     * 
     * @param void
     * 
     * @return bool 
     */
    final public static function isAvailable(): bool
    {
        return (function_exists('apcu_enabled') && true === apcu_enabled());
    }

    /**
     * Create a new APCu cache (if APCu available).
     * 
     * @param string $name Name od Cache. 
     * 
     * @return self
     */
    final public function __construct(string $name = 'default')
    {
        $this->name = $name;
        if (!self::isAvailable()) {
            throw new Exception('PHP can\'t build APCu : it\'s not available. Install or enable PHP extention APCu.');
            return;
        }
    }

    final public function set(int|string $name, mixed $content, ?int $ttl = 0): bool
    {
        return apcu_store($this->name . '::' . $name, $content, $ttl);
    }

    final public function get(int|string $name, bool &$success): mixed
    {
        return apcu_fetch($name, $success);
    }

    final public function isset(int|string $name): bool
    {
        return apcu_exists($this->name . '::' . $name);
    }

    final public function unset(int|string $name): mixed
    {
        return apcu_delete($this->name . '::' . $name);
    }

    final public function clear(): bool
    {
        return apcu_clear_cache();
    }

    final public function map(callable $callable): void
    {
        foreach(new APCUIterator as $key => $value){
            $callable($value, $key);
        }
    }

    final public function dump(): array
    {
        return true;
    }

    final public function save(): bool
    {
        return true;
    }

}
