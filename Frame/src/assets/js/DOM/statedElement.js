export class statedElement {
	state = {};
	bind = {};

	costructor(obj) {
		this.obj = obj;
		this.render();
	}

	binding(elemName, stateName) {
		this.bind[elemName] = stateName;
	}

	setState(object) {
		this.state = object;
		this.render();
	}

	#render() {
		for (const key in this.bind) {
			if(['value','innerText','innerHTML'].contains(key)){
				this.obj[key] = this.state[key];
			}
		}
	}
}

// class MyElelemnt extends statedElement {

// 	/**
// 	 * 	@var Element obj
// 	 */
// 	obj;

// 	constructor(obj) {
// 		super(obj);
// 		this.obj;
// 		this.state = {
// 			text:'foo'
// 		}
// 	}

// 	render() {
// 		this.obj.innerText = this.state.text;
// 	}
// }
