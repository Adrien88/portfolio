export class InnerPageNavigation {
	constructor() {
		this.index = document.getElementByNameId(
			"navbarSupportedContent"
		).children[0];
		for (const section of document.getElementsByName("asSection")) {
			const li = document.createElement("li").classList.add("nav-item");
			this.index.appendChild(li);
		}
	}
}

/**
 * App Toggler
 */
export class Toggler {
	constructor(defBg = "dark") 
	{
		this.toggler = document.querySelector("#themetoggler");
		this.toggler.addEventListener("click", this.toggle.bind(this));
		this.bodyClassList = document.body.classList;
		this.classList = this.toggler.classList;
		this.bodyClassList.add('bg-'+defBg);
		this.classList.add('bg-'+defBg);
	}

	toggle() {
		if (this.classList.contains("bg-dark")) {
			this.bodyClassList.replace("bg-dark", "bg-light");
			this.classList.replace("bg-dark", "bg-light");
		} else {
			this.bodyClassList.replace("bg-light", "bg-dark");
			this.classList.replace("bg-light", "bg-dark");
		}
	}
}
